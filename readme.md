# Clang for STM32 etc
An example project to test possibilities of using clang as a compiler for ARM based bare-metal targets.

## Used tools
Must have:  
- clang - main actor
- arm-none-eabi-gcc - for headers and libraries for embedded targets
- cmake - build tool
- python - for extracting paths from gcc

Optional:  
- renode - to emulate target MCU

## Usage with clang
Firstly, an additional object file must be build - *addition.cpp* - to create object file of name *addition.o*. The file was added to test interlinking between objects built with *arm-none-eabi-gcc* and built with *clang*. To do so run command:  
>`$ arm-none-eabi-g++ -c -o addition.o -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -std=c++20 -O0 -g -fno-exceptions -fno-unwind-tables -fno-rtti addition.cpp`

After this we can get to the point of the project. To compile test program we need to:
1. Run CMake configure command  
> `$ cmake -B build -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_TOOLCHAIN_FILE=Toolchain/toolchain_stm32f103.cmake`

2. Run build command  
>`$ cmake --build build`

## Usage with arm-none-eabi-gcc
Besides the main purpous of the project, which is to use clang to compile for an embedded target, the prepared cmake file allows also to compile the test program using arm-none-eabi-gcc. To do that:  
1. Configure cmake  
>`$ cmake -B build -DCMAKE_C_COMPILER=arm-none-eabi-gcc -DCMAKE_CXX_COMPILER=arm-none-eabi-g++ -DCMAKE_TOOLCHAIN_FILE=Toolchain/toolchain_stm32f103.cmake`
2. Run build command  
>`$ cmake --build build`

## OS Note
Project was prepared and tested under Linux environment. Running on Windows will likely need additional fiddling.
