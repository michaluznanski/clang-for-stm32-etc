#include <array>
#include <vector>
#include "stm32f1xx.h"

extern int add(int a);
extern int add(int a, int b);
extern int add(int a, int b, int c);

class Klasa
{
    int a{12};
public:
    virtual int xd() = 0;
    virtual ~Klasa() = default;
};

class Klasa1: public Klasa
{
    int a{13};
public:

    int xd() override
    {
        return a;
    }
};

struct Struktura
{
    int a, b, c;
    char hwdp[12];
    Struktura(): a{}, b{}, c{}, hwdp{"klasa\n"}
    {
        a = 1;
        volatile auto struktura = 12;
        b = 2;
        struktura = 3;
        c = 3;
    }
    ~Struktura() {;};
};

static Klasa& klasa_getter()
{
    static Klasa1 klasa_in_klasa_getter{};
    return klasa_in_klasa_getter;
}

static Struktura& struktura_getter()
{
    static Struktura struktura;
    return struktura;
}

static Klasa1 klasa1{};

static Struktura struktura{};

extern "C" int main()
{
    RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;
    GPIOC->CRH = 0b0001 << GPIO_CRH_MODE13_Pos;

    klasa1.xd();

    for (int i = 0; i < 6; ++i)
    {
        GPIOC->ODR ^= GPIO_ODR_ODR13;
        for (int j = 0; j < 200 * 1000; ++j)
            ;
    }

    add(1);
    add(1,2,3);
    add(3,65);

    Klasa1 xd{};
    int arr_basic_int[15] = {};
    std::array<int, 12> arr_int{};
    auto struktura = new Struktura{};
    auto struktury = new Struktura[20];

    std::vector<int> vec;
    // vec.at(2137);

    auto& klasa_getted = klasa_getter();
    auto& struktura_getted = struktura_getter();

    delete[] struktury;
    delete struktura;

    while (true)
    {
        GPIOC->ODR ^= GPIO_ODR_ODR13;
        for (int i = 0; i < 500 * 1000; ++i)
            ;
    }
}
