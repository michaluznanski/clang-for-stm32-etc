#include <cstdlib>
#include <cstddef>
#include <cerrno>
#include <new>

// poniższe dla wywoływaczki ctorów ze standardowej libki
#ifdef __clang__
extern "C" void _init() {;}
extern "C" void _fini() {;}
#endif

// poniższe podobno wynika z jakiegoś dość świeżego błędu w gcc
extern "C" int getentropy(void* buffer, size_t length)
{
    return ENOSYS;
}

void* operator new(std::size_t sz)
{
    if (sz == 0)
        ++sz; // avoid std::malloc(0) which may return nullptr on success
 
    void *ptr = std::malloc(sz);
    return ptr;
}
 
void* operator new[](std::size_t sz)
{
    if (sz == 0)
        ++sz; // avoid std::malloc(0) which may return nullptr on success
 
    void *ptr = std::malloc(sz);
    return ptr;
}

void operator delete  ( void* ptr ) noexcept
{
    std::free(ptr);
}

void operator delete  ( void* ptr, size_t size ) noexcept
{
    std::free(ptr);
}

void operator delete[]( void* ptr ) noexcept
{
    std::free(ptr);
}

void operator delete[]( void* ptr, size_t size ) noexcept
{
    std::free(ptr);
}

void* operator new(size_t size, std::nothrow_t) noexcept
{
    return operator new(size); 
}

void operator delete(void *p,  std::nothrow_t) noexcept
{
    operator delete(p); 
}

void* operator new[](size_t size, std::nothrow_t) noexcept
{
    return operator new(size); 
}

void operator delete[](void *p,  std::nothrow_t) noexcept
{
    operator delete(p);
}

// error handler odpalany, gdy wywoływana jest funkcja czysto wirtualna
extern "C" void __cxa_pure_virtual() { std::abort(); }
