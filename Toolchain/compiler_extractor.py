"""Helper for exctacting inlcude paths from arm-none-eabi-gcc"""
import argparse
from sys import stdout, exit
from subprocess import Popen, TimeoutExpired, PIPE, run
from signal import SIGINT
from io import BytesIO
from os import remove

def _ask_compiler_libs(compiler_name: str, additional_args: list[str]) -> BytesIO:
    command = f"{compiler_name} {' '.join(additional_args)} -print-search-dirs"
    output = run(command, capture_output=True, shell=True, executable="bash", timeout=3.)
    return BytesIO(output.stdout)

def _extract_lib_paths(compiler_output: BytesIO) -> list[bytes]:
    libraries = b""
    # look for starters
    for line in compiler_output:
        if line.startswith(b"libraries: ="):
            libraries = line.removeprefix(b"libraries: =").rstrip()
    exploded = libraries.split(b":")
    return exploded

def extract_lib_list(compiler_name: str, additional_args: list[str]) -> list[str]:
    raw = _ask_compiler_libs(compiler_name, additional_args)
    paths_list = _extract_lib_paths(raw)
    return [path.decode() for path in paths_list]

def extract_lib_string(compiler_name: str, additional_args: list[str], separator=' '):
    return separator.join(extract_lib_list(compiler_name, additional_args))

def _ask_compiler_includes(compiler_name: str, additional_args: list[str]) -> BytesIO:
    command = f"echo | $({compiler_name} -print-prog-name=cc1plus) -v -imultilib $({compiler_name} {' '.join(additional_args)} -print-multi-directory)"
    output = run(command, stderr=PIPE, shell=True, executable="bash", timeout=3.)
    remove('<stdin>.s') # remove artifact
    return BytesIO(output.stderr)
    
def _extract_include_paths(compiler_output: BytesIO) -> list[bytes]:
    output = []
    # look for starters
    started = False
    for line in compiler_output:
        if line.rstrip() in {b'#include "..." search starts here:', b'#include <...> search starts here:'}:
            started = True
        elif started:
            if line.startswith(b' '):
                output.append(line.strip())
            elif line.startswith(b"End of search list."):
                break
            else:
                raise RuntimeError(f"Unexpected line in output met: {line}")
    return output

def extract_include_list(compiler_name: str, additional_args: list[str]) -> list[str]:
    raw = _ask_compiler_includes(compiler_name, additional_args)
    paths_list = _extract_include_paths(raw)
    return [path.decode() for path in paths_list]

def extract_include_string(compiler_name: str, additional_args: list[str], separator=' '):
    return separator.join(extract_include_list(compiler_name, additional_args))

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("target", choices={"inc", "lib"}, help="What needs to be extracted: include or library paths")
    parser.add_argument("-c", "--compiler", help="Name of compiler to ask", default="arm-none-eabi-gcc")
    parser.add_argument("-s", "--separator", help="String to separate returned list of paths", default=' ')
    parser.add_argument("-o", "--arch-options", nargs='+', help="Architecture specific options to pass to compiler", default=["-mcpu=cortex-m3", "-mthumb", "-mfloat-abi=soft"])

    args = parser.parse_args()
    if args.target == "inc":
        print(extract_include_string(args.compiler, args.arch_options, args.separator), file=stdout, end="")
    elif args.target == "lib":
        print(extract_lib_string(args.compiler, args.arch_options, args.separator), file=stdout, end="")
    else:
        raise RuntimeError("Need to specify whether libraries or includes have to be extracted")
    exit(0)

if __name__ == "__main__":
    main()

