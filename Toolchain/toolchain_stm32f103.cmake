# na podstawie: https://cmake.org/cmake/help/book/mastering-cmake/chapter/Cross%20Compiling%20With%20CMake.html
set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_TRY_COMPILE_TARGET_TYPE "STATIC_LIBRARY") # by ominąć sprawdzenie przykładowego programu, na czym się kładzie

## czyli gdzie find_costam będzie zaczynać poszukiwania i szukać, można ustawić na więcej niż jedną
## TODO generalnie to trzeba to ustawić tam, gdzie są libki
## a nie na ścieżce do clanga
#set(CMAKE_FIND_ROOT_PATH  ${CLANG})
#
## nigdy nie używaj ustawionej powyżej ścieżki w find_program i pochodnych go używających
#set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
## zawsze używaj ustawionej powyżej ścieżki w find_library
#set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
## zawsze używaj ustawionej powyżej ścieżki w find_path i find_file
#set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
## trzecia droga - both - najpierw szukaj z użyciem prefiksu(ustawionej ścieżki)
## jeśli nie wyjdzie to domyślnej ścieżki
