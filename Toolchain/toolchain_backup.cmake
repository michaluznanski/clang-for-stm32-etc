# na podstawie: https://cmake.org/cmake/help/book/mastering-cmake/chapter/Cross%20Compiling%20With%20CMake.html
set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_VERSION Cortex-M0plus-STM32G070) # w sumie dowolne, nie trzeba ustawiać

# poniższe po stestowaniu -DCMAKE_...=clang itd.
find_program(ARM_GCC arm-none-eabi-gcc)
find_program(ARM_G++ arm-none-eabi-g++)
if (NOT ARM_GCC)
    message(SEND_ERROR ${ARM_GCC})
    message(FATAL_ERROR "Nie znaleziono armowego gcc")
endif()
if (NOT ARM_G++)
    message(FATAL_ERROR "Nie znaleziono armowego g++")
endif()

set(CMAKE_C_COMPILER ${ARM_GCC})
set(CMAKE_CXX_COMPILER ${ARM_G++})
set(CMAKE_TRY_COMPILE_TARGET_TYPE "STATIC_LIBRARY") # by ominąć sprawdzenie przykładowego programu, na czym się kładzie

add_compile_options("-mcpu=cortex-m0plus" "-mthumb" "-mfloat-abi=soft" "-mno-unaligned-access")
# bez poniższego linkuje nasz thumbowy (git) kod z jakimiś dziwnymi libkami zawierającymi armowe
# instrukcje. Lub w jakiś inny sposób zaburza te istniejące, przez co wali faultami
add_link_options("-mcpu=cortex-m0plus" "-mfloat-abi=soft") # "SHELL:-T ${CMAKE_CURRENT_SOURCE_DIR}/toolchain_g4_gcc/STM32G070xx.ld")

# ustaw startup
set(STARTUP_FILE ${CMAKE_CURRENT_SOURCE_DIR}/toolchain_g4_gcc/startup_stm32g070xx.s)

if (WIN32)
    set(COMPILER_PATH "C:/Program Files/msys64/mingw64/")
else ()
    set(COMPILER_PATH "/usr/arm-none-eabi")
endif()
# czyli gdzie find_costam będzie zaczynać poszukiwania i szukać, można ustawić na więcej niż jedną
set(CMAKE_FIND_ROOT_PATH  
    ${COMPILER_PATH}
)

# nigdy nie używaj ustawionej powyżej ścieżki w find_program i pochodnych go używających
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# zawsze używaj ustawionej powyżej ścieżki w find_library
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
# zawsze używaj ustawionej powyżej ścieżki w find_path i find_file
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
# trzecia droga - both - najpierw szukaj z użyciem prefiksu(ustawionej ścieżki)
# jeśli nie wyjdzie to domyślnej ścieżki
